﻿' Project:P151B_Train2
' Auther:IE2A No.24, 村田直人
' Date: 2015年05月08日

Public Class Form1

    '配列の要素数を宣言
    Const Last As Integer = 9

    Dim LabeIntTop As Integer 'ラベルのY座標の初期値を格納する変数
    Dim CopyDistance As Integer 'ラベルの配列間隔を格納する変数
    Dim FormInitSize As Size 'フォームのサイズ（初期値）を格納しておく変数

    'ボタンのテキスト変数
    Const Startstr As String = "スタート"
    Const Resetstr As String = "リセット"


    Dim TrainLabels(Last) As Label          'ラベル配列の要素数をLastを使って宣言
    Dim TrainRandom As Random = New Random  'Randomクラスのインスタンス化

    Dim FinishPosition As Integer           '電車ラベルの終了位置(ゴール)を格納する変数
    Dim FinishedTrains(Last) As Boolean     'ラベルごとの終了状況を格納する配列
    Dim FinishedCounter As Integer          'ゴールしたラベルの数をカウントする変数


    'フォームが呼び出された時の処理
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load


        LabeIntTop = TrainLabel.Location.Y          'ラベルY座標を変数に格納
        CopyDistance = TrainLabel.Size.Width + 12   'ラベルの配置間隔変数にラベルサイズ＋12を格納

        'フォームプロパティの設定
        With Me
            .Size = New Size(.Size.Width + CopyDistance * Last, .Size.Height) 'フォームサイズの横幅を設定する
            .MinimumSize = New Size(.Size.Width, 250)       'フォームの最少サイズを「200」に設定
            .MaximumSize = New Size(.Size.Width, 500)       'フォーム(縦)の最大サイズを「400」に設定
        End With

        TrainLabels(0) = TrainLabel 'ラベルの参照をTrainLabelsの先頭の要素に代入

        '配列にラベルを格納してプロパティを設定する処理
        For i As Integer = 1 To Last

            TrainLabels(i) = New Label 'ラベルのインスタンス化して配列に格納

            'ラベルのプロパティ設定
            With TrainLabels(i)
                .Image = TrainLabel.Image                   'イメージ設定
                .Size = TrainLabel.Size                     'サイズ設定
                .Location = New Point(TrainLabels(i - 1).Location.X + CopyDistance, LabeIntTop) 'ロケーション設定
                .ImageAlign = ContentAlignment.MiddleCenter 'ラベルの位置を設定
                .TextAlign = TrainLabel.TextAlign           'Textの表示位置を設定
            End With
        Next

        Panel1.Controls.AddRange(TrainLabels) 'パネルにラベルを追加

        'Panel1.ControlCollection.AddRange(TrainLabels)

        StartButton.Text = Startstr         'ボタンのテキストに「スタート」を表示

    End Sub

    'スタート、リセットボタン
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        'onClick時の処理分岐
        If StartButton.Text = Startstr Then

            StartButton.Text = Resetstr 'ボタンのテキスト変更
            Timer1.Start()              'タイマー起動

            FinishPosition = Panel1.ClientSize.Height - TrainLabel.Size.Height '移動終了位置を格納

        Else

            StartButton.Text = Startstr 'ボタンのテキストを変更
            Timer1.Stop()               'タイマーを停止

            'ラベルのY座標を初期化する処理
            For Each tLabel As Label In TrainLabels

                With tLabel
                    .Text = ""                                           'ラベルのTextをクリアする
                    .Location = New Point(tLabel.Location.X, LabeIntTop) 'ロケーションを設定
                End With

            Next

            Me.Size = Me.MinimumSize 'フォームのサイズを最少に戻す

            Array.Clear(FinishedTrains, 0, Last + 1)    '配列のクリア(全部False)
            FinishedCounter = 0                         '変数のクリア

        End If

    End Sub

    'タイマー処理
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Dim n As Integer = TrainRandom.Next(Last + 1) 'ランダムな数字を格納

        'ゴールしていたら抜ける
        If FinishedTrains(n) Then

            Exit Sub

        End If


        'ランダムにラベルを下げる
        With TrainLabels(n)
            .Location = New Point(.Location.X, .Location.Y + 5) 'ロケーションを設定

            '終了位置に達していないときは処理を抜ける
            If .Location.Y < FinishPosition Then
                Exit Sub
            End If
            FinishedTrains(n) = True                            '終了フラグを格納
            FinishedCounter += 1                                'カウンタをインクリメント
            .Text = FinishedCounter.ToString                    '順位を表示

        End With

        '全部ゴールしたらタイマー停止
        If FinishedCounter > Last Then

            Timer1.Stop()

        End If

    End Sub
End Class
